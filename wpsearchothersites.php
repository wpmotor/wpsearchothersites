<?php
/*
Plugin Name:    WP Search Other Sites by WPmotor.no
Plugin URI:     http://plugins.wpmotor.no/
Description:    WP Search Syndication allows one of your sites to show search results from all the other sites.
Version:        0.1
Author:         Frode Børli
License:        GPLv3
Domain Path:    Languages
Text Domain:    wpsos
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Makes search results shared between instances of WordPress that has this plugin installed.
 * 
 * To get search results from the other sites:
 * 
 *      $results = wpsos_get_search_results();
 * 
 * This will return an array with the following structure:
 * 
 *      array(
 *          "http://some-site.com/" => array(
 *              0 => array(
 *                  "the_ID" => "the_ID()",
 *                  "the_guid" => "the_guid()",
 *                  "the_content" => "the_content()",
 *                  "post_class" => "post_class()",
 *                  "the_title" => "the_title()",
 *                  "the_excerpt" => "the_excerpt()",
 *                  "the_post_thumbnail" => "the_post_thumbnail()",
 *                  "the_permalink" => "the_permalink()",
 *                  "the_author" => "the_author()",
 *                  "the_author_link" => "the_author_link()",
 *                  "the_attachment_link" => "the_attachment_link()",
 *              ),
 *              1 => ...
 *          )
 *      )
 * 
 * There are two modes:
 * 
 * 1.   Master
 *      The Master site is where searches will be performed. The Master site does
 *      not support search API requests, but performs the search API requests.
 * 2.   Slave
 *      The Slave site will perform search queries on behalf of the master site,
 *      and the Master site will merge search results from the Slave within it's
 *      own search results.
 * 
 * Todo:
 * 
 *      Work has been done to make search results from the other sites appear
 *      merged within search results from the current site. This is not yet working
 *      very well, so we've disabled that functionality.
 * 
 * To configure WP Search Syndicate perform the following tasks:
 * 
 * 1.   On ALL sites, create a configuration value for a shared secret in your 
 *      wp-config.php file:
 * 
 *      define("WPSOS_KEY", "someArbitraryAndRandomString");
 * 
 * 2.   On the Master site, define which other sites you want to search in your
 *      wp-config.php file:
 * 
 *      define("WPSOS_SITES", serialize(array(
 *          "http://site1.root.com/",
 *          "http://site1.on-a-subfolder.com/myWordPressSite/",
 *          ));
 * 
 * Note that although PHP 5.6 allows arrays in constants, we require you to use
 * the serialize function, to maintain backward compatability with earlier versions
 * of PHP.
 * 
 */
 
if(!defined("WPSOS_KEY")) {
    add_action( 'admin_notices', 'wpsos_config_missing_notice' );
    return;
}

if(defined("WPSOS_SITES")) {
    /**
     * This is a master site, and thus we have to inspect the WP_Query object whenever it is used
     */
    add_filter( 'posts_pre_query', 'wpsos_posts_pre_query', 10, 2 );
    add_action( 'init', 'wpsos_register_search_post_type');
    add_filter( 'post_type_link', 'wpsos_post_type_link', 10, 2);
} else {
    /**
     * This is a Slave site, and thus we need to create an URL that will provide the Master site with
     * search results
     */
    add_action( 'init', 'wpsos_api_endpoint' );
}

/**
 * This function receives API requests from the master site and responds 
 * appropriately
 */
function wpsos_api_endpoint() {
    if(isset($_GET['wpsos'])) {
        header("Content-Type: application/vnd.php.serialized");
        $queryData = base64_decode($_POST['data']);
        // Check hash_hmac
        $hash = hash_hmac('md5', $queryData, WPSOS_KEY);
        
        if($hash != $_GET['wpsos'])
            die(serialize(array("error" => "Hash mismatch")));

        // YAY! The hash matches, and we can securely execute the search and return any results.
        $queryData = unserialize($queryData);
        $queryData['posts_per_page'] = 1000;
        $posts = get_posts($queryData);
        $result = array();
        
        foreach($posts as $post) {
            $postA = array(
                "the_ID" => get_the_ID($post),
                "the_guid" => get_the_guid($post),
                "the_content" => get_the_content($post),
                "post_class" => get_post_class("", $post),
                "the_title" => get_the_title($post),
                "the_excerpt" => get_the_excerpt($post),
                "the_post_thumbnail" => get_the_post_thumbnail($post),
                "the_permalink" => get_the_permalink($post),
                "the_author" => get_the_author($post),
                "the_author_link" => get_the_author_link($post),
                "the_attachment_link" => get_the_attachment_link($post),
                );
            $result[] = $postA;
        }
        echo serialize($result);
        
        die();
    }
}

function wpsos_post_type_link($post_link, $post) {
    if(isset($post->_wpsos_permalink))
        return $post->_wpsos_permalink;
    return $post_link;
}

function wpsos_register_search_post_type() {
        register_post_type( 'wpsos_search_result', array(
            'public' => false,
            'has_archive' => false
            )
        );
}

function wpsos_get_results() {
    if(!isset($GLOBALS['wpsos_external_posts']))
        return null;
    return $GLOBALS['wpsos_external_posts'];
}

function wpsos_posts_pre_query($posts, $query) {
    if(!$query->is_search) return null;
    if($query->_wpsos_running) return null;
    if(is_admin()) return null;
/*
    $query->_wpsos_running = true;
    $query->set('posts_per_page', 100);
    $local_posts = $query->get_posts();
    $query->_wpsos_running = false;
*/
    $GLOBALS['wpsos_external_posts'] = wpsos_get_external_posts($query);
    return $posts;
    
    $result = array();
    foreach($local_posts as $post)
        $result[] = $post;
        
    $id = -1;
        
    foreach($external_posts as $post) {
        $post->ID = $id--;
        $result[] = $post;
    }

    usort($result, 'wpsos_usort');

    return $result;
}

function wpsos_get_external_posts($query) {
    $sites = unserialize(WPSOS_SITES);
    $posts_per_page = $query->get('posts_per_page');
    $query->set('posts_per_page', 100);
    $queryData = serialize($query->query);
    $query->set('posts_per_page', $posts_per_page);
    $hash = hash_hmac('md5', $queryData, WPSOS_KEY);

    $results = array();
    foreach($sites as $site) {
        $response = Requests::post(rtrim($site, '/').'/wp-load.php?wpsos='.rawurlencode($hash), array(), array(
            "data" => base64_encode($queryData),
            ));
        $results[$site] = unserialize(trim($response->body));
    } 
    return $results;
}

function wpsos_usort($a, $b) {
    if($a->post_date == $b->post_date)
        return 0;
    return ($a->post_date < $b->post_date) ? 1 : -1;
}

function wpsos_config_missing_notice() {
    ?>
    <div class="notice notice-error">
        <p><?php _e( "WP Search Other Sites not configured. Check the readme.txt file or visit <a href='http://plugins.wpmotor.no/'>plugins.wpmotor.no</a>.", "wpsos"); ?></p>
    </div>
    <?php
}
